package can

import (
	"encoding/binary"
	"fmt"
	"net"
	"time"
	"unsafe"

	"golang.org/x/sys/unix"
)

const SOL_CAN_RAW = unix.SOL_CAN_BASE + unix.CAN_RAW
const CAN_RAW_FILTER = 1

type Socket struct {
	IfName string
	Fd     int
}

// Filter defines a filter mask for a CAN Socket
type Filter struct {
	ID   uint32
	Mask uint32
}

type Frame struct {
	ID       uint32
	Extended bool
	DLC      byte
	Data     []byte
}

func NewSocket(ifName string, filters []Filter) (Socket, error) {
	s := Socket{}

	fd, err := unix.Socket(unix.AF_CAN, unix.SOCK_RAW, unix.CAN_RAW)
	if err != nil {
		return s, err
	}

	iface, err := net.InterfaceByName(ifName)
	if err != nil {
		return s, err
	}

	addr := &unix.SockaddrCAN{Ifindex: iface.Index}
	if err = unix.Bind(fd, addr); err != nil {
		return s, err
	}

	var vlen = uint(len(filters) * 8)

	// Setup CAN filters
	if vlen > 0 {
		_, _, errno := unix.Syscall6(unix.SYS_SETSOCKOPT, uintptr(fd),
			SOL_CAN_RAW, CAN_RAW_FILTER,
			uintptr(unsafe.Pointer(&filters[0])), uintptr(vlen), 0)
		if errno != 0 {
			return s, fmt.Errorf("setsockopt: %v", errno)
		}
	}

	s.Fd = fd
	s.IfName = ifName

	return s, nil
}

func (s Socket) EnableRecvOwnMsgs() error {
	return unix.SetsockoptInt(s.Fd, 101, 4, 1)
}

func (s Socket) Send(f Frame) error {
	// SocketCAN expects 16 bytes using this format:
	// byte 0 - 3:  32 bit CAN_ID + EFF/RTR/ERR flags
	// byte 4:      frame payload length in byte (0 .. 8)
	// byte 5 - 7:  reserved / padding
	// byte 8 - 16: raw data
	frameBytes := make([]byte, 16, 16)

	if f.Extended == false {
		binary.LittleEndian.PutUint32(frameBytes[0:4], f.ID)
	} else {
		binary.LittleEndian.PutUint32(frameBytes[0:4], f.ID|1<<31)
	}

	frameBytes[4] = f.DLC

	copy(frameBytes[8:], f.Data)

	_, err := unix.Write(s.Fd, frameBytes)

	return err
}

func (s Socket) Recv() (Frame, time.Time, error) {
	f := Frame{}

	// SocketCAN sends 16 bytes using this format:
	// byte 0 - 3:  32 bit CAN_ID + EFF/RTR/ERR flags
	// byte 4:      frame payload length in byte (0 .. 8)
	// byte 5 - 7:  reserved / padding
	// byte 8 - 16: raw data
	frameBytes := make([]byte, 16, 16)
	_, err := unix.Read(s.Fd, frameBytes)
	if err != nil {
		return f, time.Time{}, err
	}

	tv := unix.Timeval{}

	_, _, errno := unix.Syscall(unix.SYS_IOCTL, uintptr(s.Fd), unix.SIOCGSTAMP,
		uintptr(unsafe.Pointer(&tv)))
	if errno != 0 {
		return f, time.Time{}, fmt.Errorf("ioctl: %v", errno)
	}

	id := uint32(binary.LittleEndian.Uint32(frameBytes[0:4]))

	if (id & 0x80000000) != 0 {
		f.Extended = true
	} else {
		f.Extended = false
	}

	f.ID = id & 0x1FFFFFFF

	f.DLC = frameBytes[4]

	f.Data = make([]byte, 8, 8)
	copy(f.Data, frameBytes[8:])

	return f, time.Unix(tv.Unix()), nil
}
